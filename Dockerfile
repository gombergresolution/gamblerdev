version: '3.8'
services:
  hugo:
    image: klakegg/hugo:0.81.0
    volumes:
      - .:/usr/src/app
    command: hugo server -D -w --bind="0.0.0.0"
    ports:
      - "1313:1313"
  nginx:
    image: nginx:alpine
    volumes:
      - ./public:/usr/share/nginx/html
    ports:
      - "80:80"
    depends_on:
      - hugo
